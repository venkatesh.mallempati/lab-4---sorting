#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <time.h>
#include <sys/time.h>

static const long Num_To_Sort = 100;
static const double Scale = 1000.0 / RAND_MAX;

void qs_serial(long* ar_serial, int L_mark, int R_mark) 
{
	int i = L_mark, j = R_mark;
	int temp;
	int pivot = ar_serial[(L_mark + R_mark) / 2];

	while (i <= j) {
		while (ar_serial[i] < pivot)
			i++;
		while (ar_serial[j] > pivot)
			j--;
		if (i <= j) {
			temp = ar_serial[i];
			ar_serial[i] = ar_serial[j];
			ar_serial[j] = temp;
			i++;
			j--;
		}
	}

	if (L_mark < j){ qs_serial(ar_serial, L_mark, j);  }
	if (i< R_mark){ qs_serial(ar_serial, i, R_mark); }
}

void qs_parallel(long* ar_parallel, int L_mark, int R_mark) 
{
    long ar_chunksize = Num_To_Sort / omp_get_max_threads();
	#pragma omp parallel num_threads(omp_get_max_threads())
    {
		#pragma omp single nowait
		{
			qs_parallel_Recursive(ar_parallel, 0, Num_To_Sort-1, ar_chunksize);	
		}
    }  
}


void qs_parallel_Recursive(long* ar_parallel, int L_mark, int R_mark,int ar_chunksize) 
{
	int i = L_mark, j = R_mark;
	int temp;
	int pivot = ar_parallel[(L_mark + R_mark) / 2];	
	{
		while (i <= j) {
			while (ar_parallel[i] < pivot)
				i++;
			while (ar_parallel[j] > pivot)
				j--;
			if (i <= j) {
				temp = ar_parallel[i];
				ar_parallel[i] = ar_parallel[j];
				ar_parallel[j] = temp;
				i++;
				j--;
			}
		}
	}

	if ( ((R_mark-L_mark)<ar_chunksize) ){
		if (L_mark < j){ qs_parallel_Recursive(ar_parallel, L_mark, j, ar_chunksize); }			
		if (i < R_mark){ qs_parallel_Recursive(ar_parallel, i, R_mark, ar_chunksize); }

	}else{
		#pragma omp task 	
		{ qs_parallel_Recursive(ar_parallel, L_mark, j, ar_chunksize); }
		#pragma omp task 	
		{ qs_parallel_Recursive(ar_parallel, i, R_mark, ar_chunksize); }		
	}
}


int main() {
    long *ar_serial = malloc(sizeof(long) * Num_To_Sort);
    long *ar_parallel = malloc(sizeof(long) * Num_To_Sort);	

    long chunk_size = Num_To_Sort / omp_get_max_threads();
#pragma omp parallel num_threads(omp_get_max_threads())
    {
        int p = omp_get_thread_num();
        unsigned int seed = (unsigned int) time(NULL) + (unsigned int) p;
        long chunk_start = p * chunk_size;
        long chunk_end = chunk_start + chunk_size;
        for (long i = chunk_start; i < chunk_end; i++) {
            ar_serial[i] = (long) (rand_r(&seed) * Scale);
			ar_parallel[i] = ar_serial[i];
        }
    }

    struct timeval start, end;
	
	printf("\n\nArray BEFORE QUICK SORT - SERIAL: \n");
	for(long i = 0 ; i < Num_To_Sort; i++ ) 
	{
		printf("%ld ", ar_serial[i]);
	}
	printf("\n\n");
		
    gettimeofday(&start, NULL);
	qs_serial(ar_serial, 0, Num_To_Sort-1);		
    gettimeofday(&end, NULL);
	printf("\n\nArray AFTER QUICK SORT - SERIAL: \n");
	for(long i = 0 ; i < Num_To_Sort; i++ ) 
	{
		printf("%ld ", ar_serial[i]);
	}	
	printf("\n\n");
    printf("Timing sequential...\n");	
    printf("Took %f seconds\n\n", end.tv_sec - start.tv_sec + (double) (end.tv_usec - start.tv_usec) / 1000000);
	
	printf("\n\nArray BEFORE QUICK SORT - PARALLEL: \n");
	for(long i = 0 ; i < Num_To_Sort; i++ ) 
	{
		printf("%ld ", ar_parallel[i]);
	}
	printf("\n\n");	
    gettimeofday(&start, NULL);
	qs_parallel(ar_parallel, 0, Num_To_Sort-1);			
    gettimeofday(&end, NULL);
	printf("\n\nArray AFTER QUICK SORT - PARALLEL: \n");
	for(long i = 0 ; i < Num_To_Sort; i++ ) 
	{
		printf("%ld ", ar_parallel[i]);
	}	
	printf("\n\n");	
    printf("Timing parallel...\n");	
    printf("Took %f seconds\n\n", end.tv_sec - start.tv_sec + (double) (end.tv_usec - start.tv_usec) / 1000000);


    free(ar_serial);
    free(ar_parallel);	
    return 0;
}